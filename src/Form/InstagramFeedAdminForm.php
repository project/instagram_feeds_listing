<?php

namespace Drupal\instagram_feeds_listing\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The form manager.
 */
class InstagramFeedAdminForm extends ConfigFormBase {

  /**
   * Var.
   *
   * @var string
   */
  const INSTAGRAM_FEED_SETTINGS = 'instagram_feeds_listing.config';

  /**
   * GetEditableConfigNames.
   */
  protected function getEditableConfigNames(): array {
    return [
      static::INSTAGRAM_FEED_SETTINGS,
    ];
  }

  /**
   * GetFormId.
   */
  public function getFormId(): string {
    return 'instagram_feeds_listing_admin_form';
  }

  /**
   * BuildForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::INSTAGRAM_FEED_SETTINGS);

    $form['basic_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Basic Settings'),
    ];

    $form['basic_settings']['access_token'] = [
      '#required' => TRUE,
      '#type' => 'textarea',
      '#rows' => 2,
      '#cols' => 15,
      '#title' => t('Access token'),
      '#default_value' => $config->get('access_token'),
      '#example' => $this->t('corona.cerveza'),
    ];

    $markup = "<p>The token was updated on {$config->get('access_token_update_date')}.</p>";
    $markup .= "<p>The token will expire on {$config->get('access_token_expire_date')}.</p>";
    $markup .= "<p>The token will refresh automatically before the expiry date.</p>";
    $form['basic_settings']['access_token_update_date'] = [
      '#type' => 'markup',
      '#markup' => $markup,
    ];

    $form['basic_settings']['access_token_refresh_cron'] = [
      '#type' => 'checkbox',
      '#title' => t('Refresh the access token'),
      '#description' => $this->t('Refresh the access token with the next cron execution'),
      '#default_value' => $config->get('access_token_refresh_cron'),
    ];

    $form['custom_block_setting'] = [
      '#type' => 'fieldset',
      '#title' => t('Custom Block Settings'),
    ];

    $form['custom_block_setting']['block_title'] = [
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => $config->get('block_title'),
    ];

    $form['custom_block_setting']['block_fullhtml_title'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => t('Full HTML title'),
      '#default_value' => $config->get('block_fullhtml_title'),
    ];

    $form['custom_block_setting']['block_description'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#rows' => 3,
      '#cols' => 20,
      '#default_value' => $config->get('block_description'),
      '#description' => $this->t('Add any description information or links you want to display under the title'),
    ];

    $form['custom_block_setting']['block_text_link'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => t('Text link'),
      '#default_value' => $config->get('block_text_link'),
      '#description' => $this->t('By default text link is "Follow Us"'),
    ];

    $form['custom_block_setting']['block_url_link'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => t('Link to Instagram account'),
      '#default_value' => $config->get('block_url_link'),
    ];

    $form['custom_block_setting']['redirect_posts_to_user_profile'] = [
      '#type' => 'checkbox',
      '#title' => t('Redirect all posts to the profile of the user account'),
      '#description' => $this->t('When the user make click on a post, redirect to the user profile page instead of the posts page'),
      '#default_value' => $config->get('redirect_posts_to_user_profile'),
    ];

    $form['custom_block_setting']['block_class_names'] = [
      '#type' => 'textfield',
      '#title' => t('Class names'),
      '#default_value' => $config->get('block_class_names'),
    ];

    $form['custom_block_setting']['feed_limit'] = [
      '#type' => 'number',
      '#title' => t('Feed Limit'),
      '#default_value' => $config->get('feed_limit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * SubmitForm.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dateTime = new DrupalDateTime();

    $access_token_update_date = empty($form_state->getValue('access_token_update_date'))
      ? $dateTime->format('Y-m-d') : $form_state->getValue('access_token_update_date');

    $access_token_expire_date = empty($form_state->getValue('access_token_expire_date'))
      ? $dateTime->add(new \DateInterval("P60D"))->format('Y-m-d') : $form_state->getValue('access_token_expire_date');

    if ($form_state->getValue('access_token_refresh_cron') === 1) {
      $access_token_expire_date = date('Y-m-d');
      \Drupal::messenger()->addMessage('The token will be refreshed with the next cron execution');
    }

    $this->configFactory->getEditable(static::INSTAGRAM_FEED_SETTINGS)
      ->set('access_token', $form_state->getValue('access_token'))
      ->set('access_token_update_date', $access_token_update_date)
      ->set('access_token_expire_date', $access_token_expire_date)
      ->set('access_token_refresh_cron', $form_state->getValue('access_token_refresh_cron'))
      ->set('block_title', $form_state->getValue('block_title'))
      ->set('block_fullhtml_title', $form_state->getValue('block_fullhtml_title')['value'])
      ->set('block_description', $form_state->getValue('block_description'))
      ->set('block_text_link', $form_state->getValue('block_text_link'))
      ->set('block_url_link', $form_state->getValue('block_url_link'))
      ->set('redirect_posts_to_user_profile', $form_state->getValue('redirect_posts_to_user_profile'))
      ->set('block_class_names', $form_state->getValue('block_class_names'))
      ->set('feed_limit', $form_state->getValue('feed_limit'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
