<?php

namespace Drupal\instagram_feeds_listing;

/**
 * The interface manager.
 */
interface InstagramBasicDisplayApiInterface {

  /**
   * Return basic information about an user profile on instagram.
   */
  public function getUserProfile(string $userId = 'me', array $fields = []) : array;

  /**
   * Return an array with the medias of an user.
   */
  public function getUserMedia(string $userId = 'me', array $fields = []) : array;

  /**
   * RefreshToken.
   */
  public function refreshToken();

}
