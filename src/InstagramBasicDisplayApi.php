<?php

namespace Drupal\instagram_feeds_listing;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\instagram_feeds_listing\Form\InstagramFeedAdminForm;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * The class manager.
 */
class InstagramBasicDisplayApi implements InstagramBasicDisplayApiInterface {

  /**
   * Var.
   *
   * @var mixed.
   */
  public const URL_GRAPH_INSTRAGRAM_API = 'https://graph.instagram.com';

  /**
   * Var.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Var.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Var.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * Create obj.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ClientInterface $client,
    LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    $this->client = $client;
    $this->loggerChannelFactory = $loggerChannelFactory->get('instagram_feeds_listing');
    $this->configFactory = $configFactory->get(InstagramFeedAdminForm::INSTAGRAM_FEED_SETTINGS);
  }

  /**
   * {@inheritDoc}
   */
  public function getUserMedia(string $userId = 'me', array $fields = []): array {
    try {
      $timestamp = strtotime(date('Y-m-d H:i:s'));
      $fields = !empty($fields) ? $fields : [
        'id', 'caption', 'media_url', 'media_type', 'thumbnail_url',
        'permalink',
      ];
      $fields = implode(',', $fields);

      $limit = !empty($this->configFactory->get('feed_limit')) ? 'limit=' . $this->configFactory->get('feed_limit') : '';
      $endpoint = "/{$userId}/media?fields={$fields}&timestamp={$timestamp}&access_token={$this->configFactory->get('access_token')}&$limit";
      $request = $this->client->request('GET', static::URL_GRAPH_INSTRAGRAM_API . $endpoint);

      $userMedia = Json::decode($request->getBody()->getContents());
      $userMedia = $userMedia['data'] ?? [];
    }
    catch (\Exception | GuzzleException $exception) {
      $userMedia = [];
      $this->loggerChannelFactory->error($exception->getMessage());
    }

    return $userMedia;
  }

  /**
   * GetUserProfile.
   */
  public function getUserProfile(string $userId = 'me', array $fields = []): array {
    return [];
  }

  /**
   * RefreshToken.
   */
  public function refreshToken() {
    $dateCurrent = new DrupalDateTime();
    $dateCurrent = $dateCurrent->format('Y-m-d');

    $dateBeforeTokenExpire = new DrupalDateTime($this->configFactory->get('access_token_expire_date'));
    $dateBeforeTokenExpire = $dateBeforeTokenExpire->sub(new \DateInterval('P20D'));

    if (!($dateCurrent >= $dateBeforeTokenExpire)) {
      return;
    }

    try {
      $endpoint = "/refresh_access_token?grant_type=ig_refresh_token&access_token={$this->configFactory->get('access_token')}";
      $request = $this->client->request('GET', static::URL_GRAPH_INSTRAGRAM_API . $endpoint);
      $response = Json::decode($request->getBody()->getContents());
      $config = \Drupal::service('config.factory')->getEditable(InstagramFeedAdminForm::INSTAGRAM_FEED_SETTINGS);

      $newDateTimeExpireToken = new DrupalDateTime();
      $newDateTimeExpireToken = $newDateTimeExpireToken->add(new \DateInterval('PT' . $response['expires_in'] . 'S'));
      $newDateTimeExpireToken = $newDateTimeExpireToken->format('Y-m-d');

      $config
        ->set('access_token', $response['access_token'])
        ->set('access_token_update_date', $dateCurrent)
        ->set('access_token_expire_date', $newDateTimeExpireToken)
        ->save();

      $this->loggerChannelFactory->info("Access token update to  {$response['access_token']} <br> The token expire on {$newDateTimeExpireToken}");
    }
    catch (\Exception | GuzzleException $exception) {
      $this->loggerChannelFactory->error($exception->getMessage());
    }
  }

}
