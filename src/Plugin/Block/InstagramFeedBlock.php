<?php

namespace Drupal\instagram_feeds_listing\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\instagram_feeds_listing\Form\InstagramFeedAdminForm;
use Drupal\instagram_feeds_listing\InstagramBasicDisplayApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block for listing Instagram feeds.
 *
 * @Block(
 *   id = "instagram_feeds_listing_block",
 *   admin_label = @Translation("Instagram feeds listing")
 * )
 */
class InstagramFeedBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Var.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Var.
   *
   * @var \Drupal\instagram_feeds_listing\InstagramBasicDisplayApiInterface
   */
  protected $instagramBasicDisplayApi;

  /**
   * Create obj.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $configFactory,
    InstagramBasicDisplayApiInterface $instagramBasicDisplayApi,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $configFactory;
    $this->instagramBasicDisplayApi = $instagramBasicDisplayApi;
  }

  /**
   * Create.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('instagram_feeds_listing.basic_display_api')
    );
  }

  /**
   * Build.
   */
  public function build(): array {
    $configSite = $this->configFactory->get('system.site');
    $configInstagramFeed = $this->configFactory->get(InstagramFeedAdminForm::INSTAGRAM_FEED_SETTINGS);
    $userMedia = $this->instagramBasicDisplayApi->getUserMedia();

    return [
      '#theme' => 'instagram_feeds_listing__template',
      '#user_media' => $userMedia,
      '#block_title' => $configInstagramFeed->get('block_title'),
      '#block_text_link' => $configInstagramFeed->get('block_text_link'),
      '#block_url_link' => $configInstagramFeed->get('block_url_link'),
      '#block_description' => $configInstagramFeed->get('block_description'),
      '#block_class_names' => $configInstagramFeed->get('block_class_names'),
      '#redirect_posts_to_user_profile' => $configInstagramFeed->get('redirect_posts_to_user_profile'),
      '#block_fullhtml_title' => $configInstagramFeed->get('block_fullhtml_title'),
      '#site_name' => $configSite->get('name'),
    ];
  }

  /**
   * GetCacheTags.
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), [
      'instagram_feeds_listing_block',
    ]);
  }

}
